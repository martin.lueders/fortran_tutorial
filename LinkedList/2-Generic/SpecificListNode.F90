module specific_list_node_mod

  use list_node_mod
  implicit none

  private
  public :: int_list_node_t, real_list_node_t

  type, extends(list_node_t) :: int_list_node_t
    private
    integer :: value
  contains
    procedure :: get => int_list_node_get
    procedure :: is_equal => int_list_node_equal
  end type

  interface int_list_node_t
    procedure :: int_list_node_constructor
  end interface

  type, extends(list_node_t) :: real_list_node_t
    private
    real :: value
  contains
    procedure :: get => real_list_node_get
    procedure :: is_equal => real_list_node_equal
  end type

  interface real_list_node_t
    procedure :: real_list_node_constructor
  end interface

contains

  function int_list_node_constructor(next, value) result(this)
    class(list_node_t), pointer, intent(in) :: next
    integer,                     intent(in) :: value
    type(int_list_node_t), pointer :: this

    allocate(this)
    this%value = value
    call this%set_next(next)

  end function int_list_node_constructor

  function int_list_node_get(this) result(value)
    class(int_list_node_t), intent(in) :: this
    integer                            :: value

    value = this%value
  end function int_list_node_get

  function int_list_node_equal(this, other) result(equal)
    class(int_list_node_t), intent(in) :: this
    class(list_node_t),     intent(in) :: other
    logical                            :: equal

    equal = .false.
    select type(ptr => other)
    class is (int_list_node_t)
      equal = this%value == ptr%value
    end select
  end function int_list_node_equal

  function real_list_node_constructor(next, value) result(this)
    class(list_node_t), pointer, intent(in) :: next
    real,                        intent(in) :: value
    type(real_list_node_t), pointer :: this

    allocate(this)
    this%value = value
    call this%set_next(next)

  end function real_list_node_constructor

  function real_list_node_get(this) result(value)
    class(real_list_node_t), intent(in) :: this
    real                               :: value

    value = this%value
  end function real_list_node_get

  function real_list_node_equal(this, other) result(equal)
    class(real_list_node_t), intent(in) :: this
    class(list_node_t),     intent(in) :: other
    logical                            :: equal

    real, parameter :: tolerance = 0.001

    equal = .false.
    select type(ptr => other)
    class is (real_list_node_t)
      equal = abs(this%value - ptr%value) < tolerance
    end select
  end function real_list_node_equal

end module specific_list_node_mod

