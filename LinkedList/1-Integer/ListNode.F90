module list_node_mod

  implicit none

  private
  public :: list_node_t

  type list_node_t
  private
    integer :: value
    type(list_node_t), pointer :: next => null()
  contains
    procedure :: get => list_node_get
    procedure :: set_next => list_node_set_next
    procedure :: get_next => list_node_get_next
    generic :: operator(==) => is_equal
    procedure :: is_equal => list_node_equal
  end type list_node_t

  interface list_node_t
    module procedure list_node_constructor
  end interface

contains

  function list_node_constructor(next, value) result(this)
    type(list_node_t), pointer, intent(in) :: next
    integer,                    intent(in) :: value
    type(list_node_t), pointer             :: this

    allocate(this)
    this%value = value
    this%next => next

  end function list_node_constructor


  pure function list_node_get(this) result(value)
    class(list_node_t), intent(in) :: this
    integer                        :: value

    value = this%value
  end function list_node_get

  subroutine list_node_set_next(this, next)
    class(list_node_t), intent(inout) :: this
    type(list_node_t),  pointer       :: next

    this%next => next
  end subroutine list_node_set_next

  function list_node_get_next(this) result(next)
    class(list_node_t), intent(in) :: this
    type(list_node_t),  pointer    :: next

    next => this%next
  end function list_node_get_next

  function list_node_equal(this, other) result(equal)
    class(list_node_t), intent(in) :: this
    class(list_node_t), intent(in) :: other
    logical                        :: equal

    equal = this%value == other%value
  end function list_node_equal


end module list_node_mod